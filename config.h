/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;    /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const char *fonts[]          = { "monospace:size=14" };
static const char dmenufont[]       = "monospace:size=14";
static char normbgcolor[]           = "#2e3440";
static char normbordercolor[]       = "#2e3440";
static char normfgcolor[]           = "#d8dee9";
static char selfgcolor[]            = "#d8dee9";
static char selbordercolor[]        = "#d8dee9";
static char selbgcolor[]            = "#5381ac";
static char *colors[][3] = {
	   /*               fg           bg           border   */
	   [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
	   [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *  WM_CLASS(STRING) = instance, class
	 *  WM_NAME(STRING) = title
	 */
	/* class               instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",              NULL,     NULL,           0,         1,          0,           1,        -1 },
	{ "Gajim",             NULL,     NULL,           0,         1,          0,           1,        -1 },
	{ "firefox",           NULL,     NULL,           0,         0,          0,          -1,        -1 },
	{ "St",                NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "st-256color",       NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "kitty",             NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "TelegramDesktop",   NULL,     NULL,           0,         1,          0,           1,        -1 },
	{ "Session",           NULL,     NULL,           0,         1,          0,           1,        -1 },
	{ NULL,                NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",      dwindle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
};

/* key definitions */
#define BROWSER "firefox"
#define TERMINAL "kitty"
#define MODKEY Mod4Mask
#define INFOKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selfgcolor, "-sf", selbgcolor, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

// #include <X11/XF86keysym.h>

#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key            function        argument */

	/* system & patches */
	{ MODKEY,                       XK_d,          spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_b,          togglebar,      {0} },
	{ MODKEY,                       XK_j,          focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,          focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,          movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,          movestack,      {.i = -1 } },
	{ MODKEY,                       XK_o,          incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,          incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,          setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,          setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_space,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,        view,           {0} },
	{ MODKEY,                       XK_q,          killclient,     {0} },
	{ MODKEY,                       XK_t,          setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_t,          setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_i,          setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_y,          setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_y,          setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_u,          setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,             XK_u,          setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_f,          togglefullscr,  {0} },
	/* { MODKEY,                       XK_space,      setlayout,      {0} }, */
	{ MODKEY|ShiftMask,             XK_space,      togglefloating, {0} },
	{ MODKEY,                       XK_0,          view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,          tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_Left,       focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_Right,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Left,       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Right,      tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_g,          setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_g,          setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,      setgaps,        {.i = 0  } },
	{ MODKEY,                       XK_F5,         xrdb,           {.v = NULL } },
	TAGKEYS(                        XK_1,                          0)
	TAGKEYS(                        XK_2,                          1)
	TAGKEYS(                        XK_3,                          2)
	TAGKEYS(                        XK_4,                          3)
	TAGKEYS(                        XK_5,                          4)
	TAGKEYS(                        XK_6,                          5)
	TAGKEYS(                        XK_7,                          6)
	TAGKEYS(                        XK_8,                          7)
	TAGKEYS(                        XK_9,                          8)
	{ MODKEY|ShiftMask,             XK_q,          quit,           {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,          quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

